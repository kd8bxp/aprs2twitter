# APRS2Twitter-Archive

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/bfec7829e1b14c0a8748498ed31154bb)](https://www.codacy.com/app/kd8bxp/aprs2twitter-archived-?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=kd8bxp/aprs2twitter-archived-&amp;utm_campaign=Badge_Grade)

This project started in 2008 or 2009, some of the original files were lost. 
So in 2010 I started to post these to google code.
https://code.google.com/archive/p/aprs2twitter/
Google Code has since stopped operation - but they did archive everything.
My fear is that I may loose these files at some point, so I am creating an
archive of the code I have here.  

** IF you want to see some of the early code, please feel free to look at the early commits **

I stopped doing active development on this project in 2014, the final version of the script does appear to still work however. But I'm no longer active in supporting it or updating it. (Aug 11 2016)

** This might change at some point, but for now I have other interests to keep me busy **

Google code has archive version 5.xx to 12.xxb - earlier version were lost.

-------------------------------------------------------------------------

PHP Script that will send your APRS location to twitter and other mircoblogging sites

Script updates twitter, as to your aprs location found from APRS.fi, also updates WX information.

Requirements: PHP (at least 5.3.x) with CURL and JSON extensions. (We have found that you need to have the PHP CLI compiled version of PHP, the CGI compiled version does not work at this time)

KI6BJV Has been trying to get the script to work under Mac OSX - so far it looks like v6.3.9b should work, having a bit of issues with 6.4.0b right now. He has gotten version 7.00.0b to also work.

THESE DOCUMENTS ARE VERY VERY VERY OUTDATED, but the link is still active, to show were the project was and where it is going. (The setup has been simplified - just run "setup.php" now) NEW: Documents for version 7.xx.x can be found here: https://docs.google.com/document/edit?id=1NkWCi7LVSqFhbnXc2kUiefep3e-JtU4xqjx1fovsLqs&hl=en (incomplete, but all of the basics are there)

Current Script version is 13b: New downloads have been moved to: https://drive.google.com/folderview?id=0B56bYmgvVQkJcDhob05LNFJQMms&usp=sharing

I am currently moving the script to Github.com.

If you find this useful, consider giving me a small donation, anything at all would go a long way to help out. https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VAMQC4NLFZ344

Thanks, Enjoy - LeRoy, KD8BXP (I can be found on twitter KD8BXP, or you can try to catch me on APRS also KD8BXP)

much thanks to all of the Hams that have helped with this project, just a few other honorable mentions: N4TRQ for doing a lot of testing in the early days of the script, KC8QCH for hosting a oAuth file for foursquare, much thanks! and Everyone who has reported a bug or pointed out something not working, keeping me on my toes! LOL

APRS copyright (c) Bob Burninga, WB4APR, APRS2Twitter Script makes use of the following APIs with much Thanks. tinygeocoder.com, Yahoo Business Local Search API, APRS.FI API, Tumblr API, Status.net API, Instamapper.com API, Google Latitude, Nearby.org.UK API, Qaiku.com API, Weather Bug API, Amateur-Radio.net, Supertweet.net API, and Twitter, Teleku.com API With respect to all copyright holders

